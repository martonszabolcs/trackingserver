# trackingserver v0.0.0



- [Auth](#auth)
	- [Authenticate](#authenticate)
	
- [Coordinate](#coordinate)
	- [Create coordinate](#create-coordinate)
	- [Delete coordinate](#delete-coordinate)
	- [Retrieve coordinate](#retrieve-coordinate)
	- [Retrieve coordinates](#retrieve-coordinates)
	- [Update coordinate](#update-coordinate)
	
- [Geolocations](#geolocations)
	- [Create geolocations](#create-geolocations)
	- [Delete geolocations](#delete-geolocations)
	- [Retrieve geolocations](#retrieve-geolocations)
	- [Update geolocations](#update-geolocations)
	
- [Place](#place)
	- [Create place](#create-place)
	- [Delete place](#delete-place)
	- [Retrieve place](#retrieve-place)
	- [Retrieve places](#retrieve-places)
	- [Update place](#update-place)
	
- [User](#user)
	- [Create user](#create-user)
	- [Delete user](#delete-user)
	- [Retrieve current user](#retrieve-current-user)
	- [Retrieve user](#retrieve-user)
	- [Retrieve users](#retrieve-users)
	- [Update password](#update-password)
	- [Update user](#update-user)
	


# Auth

## Authenticate



	POST /auth

### Headers

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| Authorization			| String			|  <p>Basic authorization with email and password.</p>							|

### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>Master access_token.</p>							|

# Coordinate

## Create coordinate



	POST /coordinates


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| coords			| 			|  <p>Coordinate's coords.</p>							|

## Delete coordinate



	DELETE /coordinates/:id


## Retrieve coordinate



	GET /coordinates/:id


## Retrieve coordinates



	GET /coordinates


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| q			| String			| **optional** <p>Query to search.</p>							|
| page			| Number			| **optional** <p>Page number.</p>							|
| limit			| Number			| **optional** <p>Amount of returned items.</p>							|
| sort			| String[]			| **optional** <p>Order of returned items.</p>							|
| fields			| String[]			| **optional** <p>Fields to be returned.</p>							|

## Update coordinate



	PUT /coordinates/:id


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| coords			| 			|  <p>Coordinate's coords.</p>							|

# Geolocations

## Create geolocations



	POST /geolocations


## Delete geolocations



	DELETE /geolocations/:id


## Retrieve geolocations



	GET /geolocations


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| q			| String			| **optional** <p>Query to search.</p>							|
| page			| Number			| **optional** <p>Page number.</p>							|
| limit			| Number			| **optional** <p>Amount of returned items.</p>							|
| sort			| String[]			| **optional** <p>Order of returned items.</p>							|
| fields			| String[]			| **optional** <p>Fields to be returned.</p>							|

## Update geolocations



	PUT /geolocations/:id


# Place

## Create place



	POST /places


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| place			| 			|  <p>Place's place.</p>							|
| places			| 			|  <p>Place's places.</p>							|
| coordinates			| 			|  <p>Place's coordinates.</p>							|

## Delete place



	DELETE /places/:id


## Retrieve place



	GET /places/:id


## Retrieve places



	GET /places


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| q			| String			| **optional** <p>Query to search.</p>							|
| page			| Number			| **optional** <p>Page number.</p>							|
| limit			| Number			| **optional** <p>Amount of returned items.</p>							|
| sort			| String[]			| **optional** <p>Order of returned items.</p>							|
| fields			| String[]			| **optional** <p>Fields to be returned.</p>							|

## Update place



	PUT /places/:id


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| place			| 			|  <p>Place's place.</p>							|
| places			| 			|  <p>Place's places.</p>							|
| coordinates			| 			|  <p>Place's coordinates.</p>							|

# User

## Create user



	POST /users


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>Master access_token.</p>							|
| email			| String			|  <p>User's email.</p>							|
| password			| String			|  <p>User's password.</p>							|
| name			| String			| **optional** <p>User's name.</p>							|
| picture			| String			| **optional** <p>User's picture.</p>							|
| role			| String			| **optional** <p>User's role.</p>							|

## Delete user



	DELETE /users/:id


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>User access_token.</p>							|

## Retrieve current user



	GET /users/me


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>User access_token.</p>							|

## Retrieve user



	GET /users/:id


## Retrieve users



	GET /users


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>User access_token.</p>							|
| q			| String			| **optional** <p>Query to search.</p>							|
| page			| Number			| **optional** <p>Page number.</p>							|
| limit			| Number			| **optional** <p>Amount of returned items.</p>							|
| sort			| String[]			| **optional** <p>Order of returned items.</p>							|
| fields			| String[]			| **optional** <p>Fields to be returned.</p>							|

## Update password



	PUT /users/:id/password

### Headers

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| Authorization			| String			|  <p>Basic authorization with email and password.</p>							|

### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| password			| String			|  <p>User's new password.</p>							|

## Update user



	PUT /users/:id


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| access_token			| String			|  <p>User access_token.</p>							|
| name			| String			| **optional** <p>User's name.</p>							|
| picture			| String			| **optional** <p>User's picture.</p>							|


