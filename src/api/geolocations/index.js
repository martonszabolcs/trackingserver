import { Router } from 'express'
import { middleware as query } from 'querymen'
import { create, index, show, update, destroy } from './controller'
export Geolocations, { schema } from './model'
import { schema } from "./model";



const router = new Router()

/**
 * @api {post} /geolocations Create geolocations
 * @apiName CreateGeolocations
 * @apiGroup Geolocations
 * @apiSuccess {Object} geolocations Geolocations's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Geolocations not found.
 */
router.post('/',
  create)

/**
 * @api {get} /geolocations Retrieve geolocations
 * @apiName RetrieveGeolocations
 * @apiGroup Geolocations
 * @apiUse listParams
 * @apiSuccess {Object[]} geolocations List of geolocations.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 */
router.get('/',
  query(),
  index)

/**
 * @api {get} /geolocations/:id Retrieve geolocations
 * @apiName RetrieveGeolocations
 * @apiGroup Geolocations
 * @apiSuccess {Object} geolocations Geolocations's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Geolocations not found.
 */
router.get('/:id',
  show)

/**
 * @api {put} /geolocations/:id Update geolocations
 * @apiName UpdateGeolocations
 * @apiGroup Geolocations
 * @apiSuccess {Object} geolocations Geolocations's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Geolocations not found.
 */
router.put('/:id',
  update)

/**
 * @api {delete} /geolocations/:id Delete geolocations
 * @apiName DeleteGeolocations
 * @apiGroup Geolocations
 * @apiSuccess (Success 204) 204 No Content.
 * @apiError 404 Geolocations not found.
 */
router.delete('/:id',
  destroy)

export default router
