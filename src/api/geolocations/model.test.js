import { Geolocations } from '.'

let geolocations

beforeEach(async () => {
  geolocations = await Geolocations.create({})
})

describe('view', () => {
  it('returns simple view', () => {
    const view = geolocations.view()
    expect(typeof view).toBe('object')
    expect(view.id).toBe(geolocations.id)
    expect(view.createdAt).toBeTruthy()
    expect(view.updatedAt).toBeTruthy()
  })

  it('returns full view', () => {
    const view = geolocations.view(true)
    expect(typeof view).toBe('object')
    expect(view.id).toBe(geolocations.id)
    expect(view.createdAt).toBeTruthy()
    expect(view.updatedAt).toBeTruthy()
  })
})
