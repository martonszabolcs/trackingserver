import mongoose, { Schema } from 'mongoose'

const geolocationsSchema = new Schema(
  {
    geolocations: {
      type: String
    }
  },
  { timestamps: true }
);

geolocationsSchema.methods = {
  view (full) {
    const view = {
      // simple view
      id: this.id,
      createdAt: this.createdAt,
      updatedAt: this.updatedAt
    }

    return full ? {
      ...view
      // add properties for a full view
    } : view
  }
}

const model = mongoose.model('Geolocations', geolocationsSchema)

export const schema = model.schema
export default model
