import request from 'supertest'
import { apiRoot } from '../../config'
import express from '../../services/express'
import routes, { Geolocations } from '.'

const app = () => express(apiRoot, routes)

let geolocations

beforeEach(async () => {
  geolocations = await Geolocations.create({})
})

test('POST /geolocations 201', async () => {
  const { status, body } = await request(app())
    .post(`${apiRoot}`)
  expect(status).toBe(201)
  expect(typeof body).toEqual('object')
})

test('GET /geolocations 200', async () => {
  const { status, body } = await request(app())
    .get(`${apiRoot}`)
  expect(status).toBe(200)
  expect(Array.isArray(body)).toBe(true)
})

test('GET /geolocations/:id 200', async () => {
  const { status, body } = await request(app())
    .get(`${apiRoot}/${geolocations.id}`)
  expect(status).toBe(200)
  expect(typeof body).toEqual('object')
  expect(body.id).toEqual(geolocations.id)
})

test('GET /geolocations/:id 404', async () => {
  const { status } = await request(app())
    .get(apiRoot + '/123456789098765432123456')
  expect(status).toBe(404)
})

test('PUT /geolocations/:id 200', async () => {
  const { status, body } = await request(app())
    .put(`${apiRoot}/${geolocations.id}`)
  expect(status).toBe(200)
  expect(typeof body).toEqual('object')
  expect(body.id).toEqual(geolocations.id)
})

test('PUT /geolocations/:id 404', async () => {
  const { status } = await request(app())
    .put(apiRoot + '/123456789098765432123456')
  expect(status).toBe(404)
})

test('DELETE /geolocations/:id 204', async () => {
  const { status } = await request(app())
    .delete(`${apiRoot}/${geolocations.id}`)
  expect(status).toBe(204)
})

test('DELETE /geolocations/:id 404', async () => {
  const { status } = await request(app())
    .delete(apiRoot + '/123456789098765432123456')
  expect(status).toBe(404)
})
