import { success, notFound } from "../../services/response/";
import { Geolocations } from ".";

export const create = ({ body }, res, next) =>
  Geolocations.create(body)
    .then(geolocations => 
      geolocations.view(true)
    )
    .then(success(res, 201))
    .catch(next);

export const index = ({ querymen: { query, select, cursor } }, res, next) =>
  Geolocations.find(query, select, cursor)
    .then(geolocations => geolocations.map(geolocations => geolocations.view()))
    .then(success(res))
    .catch(next);

export const show = ({ params }, res, next) =>
  Geolocations.findById(params.id)
    .then(notFound(res))
    .then(geolocations => (geolocations ? geolocations.view() : null))
    .then(success(res))
    .catch(next);

export const update = ({ body, params }, res, next) =>
  Geolocations.findById(params.id)
    .then(notFound(res))
    .then(geolocations =>
      geolocations ? Object.assign(geolocations, body).save() : null
    )
    .then(geolocations => (geolocations ? geolocations.view(true) : null))
    .then(success(res))
    .catch(next);

export const destroy = ({ params }, res, next) =>
  Geolocations.findById(params.id)
    .then(notFound(res))
    .then(geolocations => (geolocations ? geolocations.remove() : null))
    .then(success(res, 204))
    .catch(next);
