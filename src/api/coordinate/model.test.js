import { Coordinate } from '.'

let coordinate

beforeEach(async () => {
  coordinate = await Coordinate.create({ coords: 'test' })
})

describe('view', () => {
  it('returns simple view', () => {
    const view = coordinate.view()
    expect(typeof view).toBe('object')
    expect(view.id).toBe(coordinate.id)
    expect(view.coords).toBe(coordinate.coords)
    expect(view.createdAt).toBeTruthy()
    expect(view.updatedAt).toBeTruthy()
  })

  it('returns full view', () => {
    const view = coordinate.view(true)
    expect(typeof view).toBe('object')
    expect(view.id).toBe(coordinate.id)
    expect(view.coords).toBe(coordinate.coords)
    expect(view.createdAt).toBeTruthy()
    expect(view.updatedAt).toBeTruthy()
  })
})
