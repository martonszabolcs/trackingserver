import { Router } from 'express'
import { middleware as query } from 'querymen'
import { middleware as body } from 'bodymen'
import { create, index, show, update, destroy } from './controller'
import { schema } from './model'
export Coordinate, { schema } from './model'

const router = new Router()
const { coords } = schema.tree

/**
 * @api {post} /coordinates Create coordinate
 * @apiName CreateCoordinate
 * @apiGroup Coordinate
 * @apiParam coords Coordinate's coords.
 * @apiSuccess {Object} coordinate Coordinate's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Coordinate not found.
 */
router.post('/',
  body({ coords }),
  create)

/**
 * @api {get} /coordinates Retrieve coordinates
 * @apiName RetrieveCoordinates
 * @apiGroup Coordinate
 * @apiUse listParams
 * @apiSuccess {Number} count Total amount of coordinates.
 * @apiSuccess {Object[]} rows List of coordinates.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 */
router.get('/',
  query(),
  index)

/**
 * @api {get} /coordinates/:id Retrieve coordinate
 * @apiName RetrieveCoordinate
 * @apiGroup Coordinate
 * @apiSuccess {Object} coordinate Coordinate's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Coordinate not found.
 */
router.get('/:id',
  show)

/**
 * @api {put} /coordinates/:id Update coordinate
 * @apiName UpdateCoordinate
 * @apiGroup Coordinate
 * @apiParam coords Coordinate's coords.
 * @apiSuccess {Object} coordinate Coordinate's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Coordinate not found.
 */
router.put('/:id',
  body({ coords }),
  update)

/**
 * @api {delete} /coordinates/:id Delete coordinate
 * @apiName DeleteCoordinate
 * @apiGroup Coordinate
 * @apiSuccess (Success 204) 204 No Content.
 * @apiError 404 Coordinate not found.
 */
router.delete('/:id',
  destroy)

export default router
