import mongoose, { Schema } from 'mongoose'

const coordinateSchema = new Schema({
  coords: {
    type: String
  }
}, {
  timestamps: true,
  toJSON: {
    virtuals: true,
    transform: (obj, ret) => { delete ret._id }
  }
})

coordinateSchema.methods = {
  view (full) {
    const view = {
      // simple view
      id: this.id,
      coords: this.coords,
      createdAt: this.createdAt,
      updatedAt: this.updatedAt
    }

    return full ? {
      ...view
      // add properties for a full view
    } : view
  }
}

const model = mongoose.model('Coordinate', coordinateSchema)

export const schema = model.schema
export default model
