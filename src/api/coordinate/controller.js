import { success, notFound } from '../../services/response/'
import { Coordinate } from '.'

export const create = ({ bodymen: { body } }, res, next) =>
  Coordinate.create(body)
    .then((coordinate) => coordinate.view(true))
    .then(success(res, 201))
    .catch(next)

export const index = ({ querymen: { query, select, cursor } }, res, next) =>
  Coordinate.count(query)
    .then(count => Coordinate.find(query, select, cursor)
      .then((coordinates) => ({
        count,
        rows: coordinates.map((coordinate) => coordinate.view())
      }))
    )
    .then(success(res))
    .catch(next)

export const show = ({ params }, res, next) =>
  Coordinate.findById(params.id)
    .then(notFound(res))
    .then((coordinate) => coordinate ? coordinate.view() : null)
    .then(success(res))
    .catch(next)

export const update = ({ bodymen: { body }, params }, res, next) =>
  Coordinate.findById(params.id)
    .then(notFound(res))
    .then((coordinate) => coordinate ? Object.assign(coordinate, body).save() : null)
    .then((coordinate) => coordinate ? coordinate.view(true) : null)
    .then(success(res))
    .catch(next)

export const destroy = ({ params }, res, next) =>
  Coordinate.findById(params.id)
    .then(notFound(res))
    .then((coordinate) => coordinate ? coordinate.remove() : null)
    .then(success(res, 204))
    .catch(next)
