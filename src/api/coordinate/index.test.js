import request from 'supertest'
import { apiRoot } from '../../config'
import express from '../../services/express'
import routes, { Coordinate } from '.'

const app = () => express(apiRoot, routes)

let coordinate

beforeEach(async () => {
  coordinate = await Coordinate.create({})
})

test('POST /coordinates 201', async () => {
  const { status, body } = await request(app())
    .post(`${apiRoot}`)
    .send({ coords: 'test' })
  expect(status).toBe(201)
  expect(typeof body).toEqual('object')
  expect(body.coords).toEqual('test')
})

test('GET /coordinates 200', async () => {
  const { status, body } = await request(app())
    .get(`${apiRoot}`)
  expect(status).toBe(200)
  expect(Array.isArray(body.rows)).toBe(true)
  expect(Number.isNaN(body.count)).toBe(false)
})

test('GET /coordinates/:id 200', async () => {
  const { status, body } = await request(app())
    .get(`${apiRoot}/${coordinate.id}`)
  expect(status).toBe(200)
  expect(typeof body).toEqual('object')
  expect(body.id).toEqual(coordinate.id)
})

test('GET /coordinates/:id 404', async () => {
  const { status } = await request(app())
    .get(apiRoot + '/123456789098765432123456')
  expect(status).toBe(404)
})

test('PUT /coordinates/:id 200', async () => {
  const { status, body } = await request(app())
    .put(`${apiRoot}/${coordinate.id}`)
    .send({ coords: 'test' })
  expect(status).toBe(200)
  expect(typeof body).toEqual('object')
  expect(body.id).toEqual(coordinate.id)
  expect(body.coords).toEqual('test')
})

test('PUT /coordinates/:id 404', async () => {
  const { status } = await request(app())
    .put(apiRoot + '/123456789098765432123456')
    .send({ coords: 'test' })
  expect(status).toBe(404)
})

test('DELETE /coordinates/:id 204', async () => {
  const { status } = await request(app())
    .delete(`${apiRoot}/${coordinate.id}`)
  expect(status).toBe(204)
})

test('DELETE /coordinates/:id 404', async () => {
  const { status } = await request(app())
    .delete(apiRoot + '/123456789098765432123456')
  expect(status).toBe(404)
})
