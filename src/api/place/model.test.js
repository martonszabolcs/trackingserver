import { Place } from '.'

let place

beforeEach(async () => {
  place = await Place.create({ place: 'test', places: 'test', coordinates: 'test' })
})

describe('view', () => {
  it('returns simple view', () => {
    const view = place.view()
    expect(typeof view).toBe('object')
    expect(view.id).toBe(place.id)
    expect(view.place).toBe(place.place)
    expect(view.places).toBe(place.places)
    expect(view.coordinates).toBe(place.coordinates)
    expect(view.createdAt).toBeTruthy()
    expect(view.updatedAt).toBeTruthy()
  })

  it('returns full view', () => {
    const view = place.view(true)
    expect(typeof view).toBe('object')
    expect(view.id).toBe(place.id)
    expect(view.place).toBe(place.place)
    expect(view.places).toBe(place.places)
    expect(view.coordinates).toBe(place.coordinates)
    expect(view.createdAt).toBeTruthy()
    expect(view.updatedAt).toBeTruthy()
  })
})
