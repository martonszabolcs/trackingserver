import { Router } from 'express'
import { middleware as query } from 'querymen'
import { middleware as body } from 'bodymen'
import { create, index, show, update, destroy } from './controller'
import { schema } from './model'
export Place, { schema } from './model'

const router = new Router()
const { place, places, coordinates } = schema.tree

/**
 * @api {post} /places Create place
 * @apiName CreatePlace
 * @apiGroup Place
 * @apiParam place Place's place.
 * @apiParam places Place's places.
 * @apiParam coordinates Place's coordinates.
 * @apiSuccess {Object} place Place's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Place not found.
 */
router.post('/',
  body({ place, places, coordinates }),
  create)

/**
 * @api {get} /places Retrieve places
 * @apiName RetrievePlaces
 * @apiGroup Place
 * @apiUse listParams
 * @apiSuccess {Object[]} places List of places.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 */
router.get('/',
  query(),
  index)

/**
 * @api {get} /places/:id Retrieve place
 * @apiName RetrievePlace
 * @apiGroup Place
 * @apiSuccess {Object} place Place's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Place not found.
 */
router.get('/:id',
  show)

/**
 * @api {put} /places/:id Update place
 * @apiName UpdatePlace
 * @apiGroup Place
 * @apiParam place Place's place.
 * @apiParam places Place's places.
 * @apiParam coordinates Place's coordinates.
 * @apiSuccess {Object} place Place's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Place not found.
 */
router.put('/:id',
  body({ place, places, coordinates }),
  update)

/**
 * @api {delete} /places/:id Delete place
 * @apiName DeletePlace
 * @apiGroup Place
 * @apiSuccess (Success 204) 204 No Content.
 * @apiError 404 Place not found.
 */
router.delete('/:id',
  destroy)

export default router
