import request from 'supertest'
import { apiRoot } from '../../config'
import express from '../../services/express'
import routes, { Place } from '.'

const app = () => express(apiRoot, routes)

let place

beforeEach(async () => {
  place = await Place.create({})
})

test('POST /places 201', async () => {
  const { status, body } = await request(app())
    .post(`${apiRoot}`)
    .send({ place: 'test', places: 'test', coordinates: 'test' })
  expect(status).toBe(201)
  expect(typeof body).toEqual('object')
  expect(body.place).toEqual('test')
  expect(body.places).toEqual('test')
  expect(body.coordinates).toEqual('test')
})

test('GET /places 200', async () => {
  const { status, body } = await request(app())
    .get(`${apiRoot}`)
  expect(status).toBe(200)
  expect(Array.isArray(body)).toBe(true)
})

test('GET /places/:id 200', async () => {
  const { status, body } = await request(app())
    .get(`${apiRoot}/${place.id}`)
  expect(status).toBe(200)
  expect(typeof body).toEqual('object')
  expect(body.id).toEqual(place.id)
})

test('GET /places/:id 404', async () => {
  const { status } = await request(app())
    .get(apiRoot + '/123456789098765432123456')
  expect(status).toBe(404)
})

test('PUT /places/:id 200', async () => {
  const { status, body } = await request(app())
    .put(`${apiRoot}/${place.id}`)
    .send({ place: 'test', places: 'test', coordinates: 'test' })
  expect(status).toBe(200)
  expect(typeof body).toEqual('object')
  expect(body.id).toEqual(place.id)
  expect(body.place).toEqual('test')
  expect(body.places).toEqual('test')
  expect(body.coordinates).toEqual('test')
})

test('PUT /places/:id 404', async () => {
  const { status } = await request(app())
    .put(apiRoot + '/123456789098765432123456')
    .send({ place: 'test', places: 'test', coordinates: 'test' })
  expect(status).toBe(404)
})

test('DELETE /places/:id 204', async () => {
  const { status } = await request(app())
    .delete(`${apiRoot}/${place.id}`)
  expect(status).toBe(204)
})

test('DELETE /places/:id 404', async () => {
  const { status } = await request(app())
    .delete(apiRoot + '/123456789098765432123456')
  expect(status).toBe(404)
})
